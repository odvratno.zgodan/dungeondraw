package no.none.dungeondraw.shared.servers

internal actual class UdpServer {

    actual var mapName: String = ""
    actual val localIpAddress: String = ""
    private var started: Boolean = false

    actual fun start() {
        if (!started) {
        }
    }

    actual fun stop() {
        if (started) {
        }
    }
}
