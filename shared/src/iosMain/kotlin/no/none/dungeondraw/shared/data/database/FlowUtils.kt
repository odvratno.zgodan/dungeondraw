package no.none.dungeondraw.shared.data.database

import io.ktor.utils.io.core.Closeable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import no.none.dungeondraw.shared.data.viewmodel.MapsViewModel
import no.none.dungeondraw.shared.data.vo.AppState

// This method is needed only for iOS
fun MapsViewModel.onChange(provideNewState: ((AppState) -> Unit)): Closeable {
    val job = Job()
    stateFlow.onEach {
        provideNewState(it)
    }.launchIn(
        CoroutineScope(Dispatchers.Main + job)
    )
    return object : Closeable {
        override fun close() {
            job.cancel()
        }
    }
}
