package no.none.dungeondraw.shared.data.database

import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.sqlite.driver.JdbcSqliteDriver
import no.none.dungeondraw.shared.db.AppDatabase
import java.io.File

actual fun createDriver(): SqlDriver {
    fun getApplicationCachesPath(): File {
        val osName = System.getProperty("os.name").toLowerCase()
        val file = when {
            osName.indexOf("windows") > -1 -> {
                File(System.getProperty("user.home") + "/Library/Caches/DungeonDraw")
            }
            osName.indexOf("mac") > -1 -> {
                File(System.getProperty("user.home") + "/Library/Caches/DungeonDraw")
            }
            osName.indexOf("nix") > -1 -> {
                File(System.getProperty("user.home") + "/.cache/DungeonDraw")
            }
            else -> { // anything else
                File(System.getProperty("user.home") + "/caches/DungeonDraw")
            }
        }
        if (!file.exists()) {
            try {
                file.mkdir()
            } catch (e: Exception) {
                println("e:$e")
            }
        }
        return file
    }

    return JdbcSqliteDriver("jdbc:sqlite:${getApplicationCachesPath()}/sample.db").also {
        try {
            AppDatabase.Schema.create(it)
        } catch (e: Exception) {
            println("Couldn't create DB. Exception: $e")
        }
    }
}
