package no.none.dungeondraw.shared.data.repository

import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import kotlinx.coroutines.flow.Flow
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import no.none.dungeondraw.shared.data.database.createDriver
import no.none.dungeondraw.shared.data.vo.DrawPath
import no.none.dungeondraw.shared.data.vo.FingerPath
import no.none.dungeondraw.shared.data.vo.Map
import no.none.dungeondraw.shared.db.AppDatabase

internal class MapRepository {
    private val database = AppDatabase(createDriver())
    private val dbQuery = database.appDatabaseQueries

    init {
        /*insertMap(Map("test map", Clock.System.now().toString()))*/
        /*val drawpath = DrawPath()
        drawpath.pathPoints.add(Pair(1.0f, 2.0f))
        drawpath.quadPoints.add(DrawPath.Quadruple(10.0f, 20.0f, 30.0f, 40.0f))
        insertFingerPath(FingerPath(0, 1, 0xffffff, 0f, 0f, 1.5f, drawpath))*/
    }

    internal fun getAllMaps(): Flow<List<Map>> {
        return dbQuery.maps(::mapMaps).asFlow().mapToList()
    }

    @Throws(Exception::class) internal suspend fun insertAllMaps(maps: List<Map>) {
        maps.map {
            insertMap(it)
        }
    }

    internal fun insertMap(map: Map) {
        try {
            dbQuery.insertMap(
                map.name,
                map.createdAt,
                map.imagePath
            )
        } catch (e: Exception) {
            println("exception: $e")
        }
    }

    internal fun updateMap(map: Map) {
        dbQuery.updateMap(
            map.name,
            map.createdAt,
            map.imagePath,
            map.id
        )
    }

    internal fun deleteMap(map: Map) {
        dbQuery.removeMap(
            map.id
        )
    }

    internal fun getMapFingerPaths(mapId: Long): Flow<List<FingerPath>> {
        return dbQuery.fingerPaths(mapId, ::mapFingerPaths).asFlow().mapToList()
    }

    @Throws(Exception::class) internal suspend fun insertAllFingerPaths(paths: List<FingerPath>) {
        paths.map {
            insertFingerPath(it)
        }
    }

    internal fun insertFingerPath(path: FingerPath) {
        try {
            dbQuery.insertFingerPath(
                path.mapId,
                path.color.toLong(),
                path.x,
                path.y,
                path.strokeWidth,
                path.paintStyle.name,
                Json.encodeToString(path.drawPath),
                path.createdAt
            )
        } catch (e: Exception) {
            println("exception: $e")
        }
    }

    internal fun updateFingerPath(path: FingerPath) {
        dbQuery.updateFingerPath(
            path.mapId,
            path.color.toLong(),
            path.x,
            path.y,
            path.strokeWidth,
            path.paintStyle.name,
            Json.encodeToString(path),
            path.id
        )
    }

    internal fun deleteFingerPath(path: FingerPath) {
        dbQuery.removeFingerPath(
            path.id
        )
    }

    internal fun deleteFingerPaths(mapId: Long) {
        dbQuery.removeAllFingerPaths(
            mapId
        )
    }

    private fun mapMaps(
        id: Long,
        name: String,
        createdAt: String,
        imagePath: String
    ): Map {
        return Map(
            id,
            name,
            createdAt,
            imagePath
        )
    }

    private fun mapFingerPaths(
        id: Long,
        mapId: Long,
        color: Long,
        x: Float,
        y: Float,
        strokeWidth: Float,
        paintStyle: String,
        drawPathEncoded: String,
        createdAtMillis: Long
    ): FingerPath {
        val drawPath = Json.decodeFromString<DrawPath>(drawPathEncoded)
        return FingerPath(
            id,
            mapId,
            color.toInt(),
            x,
            y,
            strokeWidth,
            FingerPath.Style.valueOf(paintStyle),
            drawPath,
            createdAtMillis
        )
    }
}
