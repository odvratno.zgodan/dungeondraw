package no.none.dungeondraw.shared.data.navigation

interface Screen

enum class MainScreen : Screen {
    LIST,
    EDIT,
    NEW,
    DRAW
}
