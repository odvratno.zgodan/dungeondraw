package no.none.dungeondraw.shared.data.statemanager

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import no.none.dungeondraw.shared.data.repository.MapRepository
import no.none.dungeondraw.shared.data.vo.AppState
import no.none.dungeondraw.shared.data.vo.FingerPath
import no.none.dungeondraw.shared.data.vo.Map
import no.none.dungeondraw.shared.data.vo.Result
import no.none.dungeondraw.shared.servers.MapImageServer
import no.none.dungeondraw.shared.servers.MapServerResponse
import no.none.dungeondraw.shared.servers.UdpServer

class MapStateManager {
    internal val mutableStateFlow: MutableStateFlow<AppState> = MutableStateFlow(
        AppState()
    )
    internal val dataRepository by lazy { MapRepository() }

    internal val server = MapImageServer()
    internal val udpServer = UdpServer()

    init {
        GlobalScope.launch {
            mutableStateFlow.collect {
                if (it.map is Result.Success && it.mapFingerPaths is Result.Success) {
                    udpServer.mapName = it.map.data.name
                    server.start()
                } else {
                    server.stop()
                }
            }
        }
        mutableStateFlow.value = mutableStateFlow.value.copy(serverAddress = Result.success(udpServer.localIpAddress))
    }
}

suspend fun MapStateManager.startUDPServer() {
    udpServer.start()
}

suspend fun MapStateManager.stopUDPServer() {
    udpServer.stop()
}

suspend fun MapStateManager.loadMaps() {
    mutableStateFlow.value = mutableStateFlow.value.copy(maps = Result.loading(), map = Result.empty(), mapFingerPaths = Result.empty())
    dataRepository.getAllMaps().collect {
        mutableStateFlow.value = mutableStateFlow.value.copy(maps = Result.success(it))
    }
}
suspend fun MapStateManager.loadMap(map: Map) {
    mutableStateFlow.value = mutableStateFlow.value.copy(map = Result.success(map))
}

suspend fun MapStateManager.saveMap(map: Map) {
    when (map.id) {
        0L -> dataRepository.insertMap(map)
        else -> dataRepository.updateMap(map)
    }
}

suspend fun MapStateManager.loadMapFingerPaths(map: Map) {
    mutableStateFlow.value = mutableStateFlow.value.copy(mapFingerPaths = Result.loading())
    dataRepository.getMapFingerPaths(map.id).collect {
        mutableStateFlow.value = mutableStateFlow.value.copy(mapFingerPaths = Result.success(it))
    }
}

suspend fun MapStateManager.saveMapFingerPath(fingerPath: FingerPath) {
    // The values of the mapFingerPaths will be automatically updated after DB operation, but we
    // want to avoid the recomposition lag in the PaintView so we update the mapFingerPaths
    // manually before the insertion
    var original = mutableListOf<FingerPath>()
    if (mutableStateFlow.value.mapFingerPaths is Result.Success) {
        original = (mutableStateFlow.value.mapFingerPaths as Result.Success).data.toMutableList()
    }
    original.add(fingerPath)
    mutableStateFlow.value = mutableStateFlow.value.copy(mapFingerPaths = Result.success(original))
    dataRepository.insertFingerPath(fingerPath)
}

suspend fun MapStateManager.deleteMapFingerPath(fingerPath: FingerPath) {
    // The values of the mapFingerPaths will be automatically updated after DB operation, but we
    // want to avoid the recomposition lag in the PaintView so we update the mapFingerPaths
    // manually before the deletion
    var original = mutableListOf<FingerPath>()
    if (mutableStateFlow.value.mapFingerPaths is Result.Success) {
        original = (mutableStateFlow.value.mapFingerPaths as Result.Success).data.toMutableList()
    }
    original.remove(fingerPath)
    mutableStateFlow.value = mutableStateFlow.value.copy(mapFingerPaths = Result.success(original))
    dataRepository.deleteFingerPath(fingerPath)
}

suspend fun MapStateManager.deleteMapFingerPaths(mapId: Long) {
    dataRepository.deleteFingerPaths(mapId)
}

suspend fun MapStateManager.setRenderedImagePath(path: String) {
    server.imagePath = path
}

suspend fun MapStateManager.searchForServers() {
    mutableStateFlow.value = mutableStateFlow.value.copy(remoteMaps = Result.loading())
    val list = mutableListOf<MapServerResponse>()
    udpServer.searchForServers(
        onDiscovered = {
            println("Device discovered: $it")
            list.add(it)
        },
        onDiscoveryFailed = {
            mutableStateFlow.value = mutableStateFlow.value.copy(remoteMaps = Result.error(it))
        },
        onDiscoveryFinished = {
            if (list.isNotEmpty()) {
                mutableStateFlow.value = mutableStateFlow.value.copy(remoteMaps = Result.success(list))
            } else {
                mutableStateFlow.value = mutableStateFlow.value.copy(remoteMaps = Result.empty())
            }
        }
    )
}
