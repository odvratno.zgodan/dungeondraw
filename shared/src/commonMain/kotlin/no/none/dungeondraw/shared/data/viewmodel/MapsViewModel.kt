package no.none.dungeondraw.shared.data.viewmodel

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import no.none.dungeondraw.shared.data.statemanager.MapStateManager
import no.none.dungeondraw.shared.data.statemanager.deleteMapFingerPath
import no.none.dungeondraw.shared.data.statemanager.deleteMapFingerPaths
import no.none.dungeondraw.shared.data.statemanager.loadMap
import no.none.dungeondraw.shared.data.statemanager.loadMapFingerPaths
import no.none.dungeondraw.shared.data.statemanager.loadMaps
import no.none.dungeondraw.shared.data.statemanager.saveMap
import no.none.dungeondraw.shared.data.statemanager.saveMapFingerPath
import no.none.dungeondraw.shared.data.statemanager.searchForServers
import no.none.dungeondraw.shared.data.statemanager.setRenderedImagePath
import no.none.dungeondraw.shared.data.statemanager.startUDPServer
import no.none.dungeondraw.shared.data.statemanager.stopUDPServer
import no.none.dungeondraw.shared.data.vo.AppState
import no.none.dungeondraw.shared.data.vo.FingerPath
import no.none.dungeondraw.shared.data.vo.Map

class MapsViewModel {

    val stateFlow: StateFlow<AppState>
        get() = stateManager.mutableStateFlow

    val stateManager by lazy { MapStateManager() }

    var serverDiscoveryJob: Job? = null
}

fun MapsViewModel.loadAllMaps() {
    GlobalScope.launch {
        stateManager.loadMaps()
    }
}

fun MapsViewModel.loadMapDetails(map: Map) {
    GlobalScope.launch {
        stateManager.loadMap(map)
        stateManager.loadMapFingerPaths(map)
    }
}

fun MapsViewModel.saveMapDetails(map: Map) {
    GlobalScope.launch {
        stateManager.saveMap(map)
    }
}

fun MapsViewModel.loadMapFingerPaths(map: Map) {
    GlobalScope.launch {
        stateManager.loadMapFingerPaths(map)
    }
}

fun MapsViewModel.saveMapFingerPath(fingerPath: FingerPath) {
    GlobalScope.launch {
        stateManager.saveMapFingerPath(fingerPath)
    }
}

fun MapsViewModel.deleteMapFingerPath(fingerPath: FingerPath) {
    GlobalScope.launch {
        stateManager.deleteMapFingerPath(fingerPath)
    }
}

fun MapsViewModel.deleteMapFingerPaths(mapId: Long) {
    GlobalScope.launch {
        stateManager.deleteMapFingerPaths(mapId)
    }
}

fun MapsViewModel.setRenderedImagePath(path: String) {
    GlobalScope.launch {
        stateManager.setRenderedImagePath(path)
    }
}

fun MapsViewModel.startUDPServer() {
    GlobalScope.launch {
        stateManager.startUDPServer()
    }
}

fun MapsViewModel.stopUDPServer() {
    GlobalScope.launch {
        stateManager.stopUDPServer()
    }
}

fun MapsViewModel.startServerDiscovery() {
    serverDiscoveryJob = GlobalScope.launch {
        while (isActive) {
            stateManager.searchForServers()
            delay(5000)
        }
    }
}

fun MapsViewModel.stopServerDiscovery() {
    serverDiscoveryJob?.cancel()
}
