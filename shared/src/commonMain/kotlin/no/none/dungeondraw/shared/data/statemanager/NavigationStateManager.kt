package no.none.dungeondraw.shared.data.statemanager

import kotlinx.coroutines.flow.MutableStateFlow
import no.none.dungeondraw.shared.data.navigation.MainScreen
import no.none.dungeondraw.shared.data.vo.NavigationState

class NavigationStateManager {
    internal val mutableStateFlow: MutableStateFlow<NavigationState> = MutableStateFlow(
        NavigationState(listOf(MainScreen.LIST))
    )
}

fun NavigationStateManager.back() {
    val screens = mutableStateFlow.value.screens.toMutableList()
    if (screens.size == 1) {
        return
    }
    screens.removeAt(screens.lastIndex)
    mutableStateFlow.value = NavigationState(screens.toList())
}

fun NavigationStateManager.draw() {
    val screens = mutableStateFlow.value.screens.toMutableSet()
    screens.add(MainScreen.DRAW)
    mutableStateFlow.value = NavigationState(screens.toList())
}

fun NavigationStateManager.new() {
    val screens = mutableStateFlow.value.screens.toMutableSet()
    screens.add(MainScreen.NEW)
    mutableStateFlow.value = NavigationState(screens.toList())
}

fun NavigationStateManager.edit() {
    val screens = mutableStateFlow.value.screens.toMutableSet()
    screens.add(MainScreen.EDIT)
    mutableStateFlow.value = NavigationState(screens.toList())
}
