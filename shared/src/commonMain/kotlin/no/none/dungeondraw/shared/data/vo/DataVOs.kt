package no.none.dungeondraw.shared.data.vo

import kotlinx.datetime.Clock
import kotlinx.serialization.Serializable
import no.none.dungeondraw.shared.data.navigation.Screen
import no.none.dungeondraw.shared.servers.MapServerResponse

data class AppState(
    val maps: Result<List<Map>> = Result.empty(),
    val map: Result<Map> = Result.empty(),
    val mapFingerPaths: Result<List<FingerPath>> = Result.empty(),
    val remoteMaps: Result<List<MapServerResponse>> = Result.empty(),
    val serverAddress: Result<String> = Result.empty()
)

data class NavigationState(val screens: List<Screen>) {
    val currentScreen = screens.last()
}

@Serializable
data class Map(
    var id: Long = 0,
    var name: String,
    var createdAt: String,
    var imagePath: String
) {
    constructor(name: String, createdAt: String) : this(0, name, createdAt, "")
    constructor() : this(0, "", Clock.System.now().toString(), "")

    override fun hashCode(): Int {
        return super.hashCode()
    }
}

@Serializable
data class FingerPath(
    var id: Long = 0,
    var mapId: Long = 0,
    val color: Int,
    val x: Float,
    val y: Float,
    val strokeWidth: Float,
    val paintStyle: Style,
    val drawPath: DrawPath,
    val createdAt: Long = Clock.System.now().toEpochMilliseconds()
) {
    enum class Style {
        FILL, STROKE
    }
}

@Serializable
class DrawPath(
    val points: MutableList<Pair<Float, Float>>,
) {
    constructor() : this(points = mutableListOf())

    /*init {
        if(pathPoints.size>0){
            super.moveTo(pathPoints.first().first, pathPoints.first().second)
            if(quadPoints.size>0){
                for(i in 0 until quadPoints.size){
                    super.quadTo(quadPoints[i].first, quadPoints[i].second, quadPoints[i].third, quadPoints[i].fourth)
                }
            }
            super.lineTo(pathPoints.last().first, pathPoints.last().second)
        }
    }

    override fun moveTo(x: Float, y: Float) {
        super.moveTo(x, y)
        pathPoints.add(Pair(x, y))

    }

    override fun lineTo(x: Float, y: Float) {
        super.lineTo(x, y)
        pathPoints.add(Pair(x, y))
    }

    override fun quadTo(x1: Float, y1: Float, x2: Float, y2: Float) {
        super.quadTo(x1, y1, x2, y2)
        quadPoints.add(Quadruple(x1, y1, x2, y2))
    }*/

    @Serializable
    data class Quadruple<out A, out B, out C, out D>(
        val first: A,
        val second: B,
        val third: C,
        val fourth: D
    )
}
