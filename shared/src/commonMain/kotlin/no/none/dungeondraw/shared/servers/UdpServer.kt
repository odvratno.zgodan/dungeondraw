package no.none.dungeondraw.shared.servers

internal expect class UdpServer() {

    var mapName: String
    val localIpAddress: String

    fun start()
    fun stop()
    fun searchForServers(
        onDiscovered: (response: MapServerResponse) -> Unit,
        onDiscoveryFinished: () -> Unit,
        onDiscoveryFailed: (e: Exception) -> Unit
    )
}

data class MapServerResponse(val name: String?, val ipAddress: String)
