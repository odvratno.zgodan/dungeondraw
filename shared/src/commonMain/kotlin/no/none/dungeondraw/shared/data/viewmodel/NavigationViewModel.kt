package no.none.dungeondraw.shared.data.viewmodel

import kotlinx.coroutines.flow.StateFlow
import no.none.dungeondraw.shared.data.statemanager.NavigationStateManager
import no.none.dungeondraw.shared.data.statemanager.back
import no.none.dungeondraw.shared.data.statemanager.draw
import no.none.dungeondraw.shared.data.statemanager.edit
import no.none.dungeondraw.shared.data.statemanager.new
import no.none.dungeondraw.shared.data.vo.NavigationState

class NavigationViewModel {

    val stateFlow: StateFlow<NavigationState>
        get() = stateManager.mutableStateFlow

    val stateManager by lazy { NavigationStateManager() }
}

fun NavigationViewModel.goBack() {
    stateManager.back()
}

fun NavigationViewModel.drawMap() {
    stateManager.draw()
}

fun NavigationViewModel.editMap() {
    stateManager.edit()
}

fun NavigationViewModel.newMap() {
    stateManager.new()
}
