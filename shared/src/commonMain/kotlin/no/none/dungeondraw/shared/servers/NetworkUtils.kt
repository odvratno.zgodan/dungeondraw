package no.none.dungeondraw.shared.servers

import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.SocketException

class NetworkUtils {

    companion object {
        @Throws(SocketException::class)
        public fun getLocalIpAddress(): InetAddress? {
            val en = NetworkInterface.getNetworkInterfaces()
            while (en.hasMoreElements()) {
                val intf = en.nextElement()
                val enumIpAddr = intf.getInetAddresses()
                while (enumIpAddr.hasMoreElements()) {
                    val inetAddress = enumIpAddr.nextElement()
                    if (!inetAddress.isLoopbackAddress() && inetAddress is Inet4Address
                    ) {
                        return inetAddress
                    }
                }
            }
            throw java.lang.IllegalStateException("Device's IP not found")
        }
    }
}
