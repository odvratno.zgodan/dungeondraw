package no.none.dungeondraw.shared.servers

internal expect class MapImageServer() {
    var imagePath: String
    fun start()
    fun stop()
}
