package no.none.dungeondraw.shared.data

sealed class Resource<out T : Any> {
    class Loading<out T : Any> : Resource<T>()
    data class Success<out T : Any>(val data: T) : Resource<T>()
    data class Error(val exception: Exception) : Resource<Nothing>()
}
