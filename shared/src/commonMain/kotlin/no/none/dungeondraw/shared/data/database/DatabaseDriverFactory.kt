package no.none.dungeondraw.shared.data.database

import com.squareup.sqldelight.db.SqlDriver

// This methods use the expect/actual mechanism to provide platform specific implementations
expect fun createDriver(): SqlDriver
