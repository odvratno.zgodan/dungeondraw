package no.none.dungeondraw.shared.servers

import com.github.aakira.napier.Napier
import com.google.gson.Gson
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.SocketTimeoutException
import kotlin.concurrent.thread

internal actual class UdpServer {

    actual var mapName: String = ""
    actual val localIpAddress: String = NetworkUtils.getLocalIpAddress().toString()
    private var started: Boolean = false
    private var udpDiscoveryClient: DatagramSocket? = null
    private var udpDiscoveryServer: DatagramSocket? = null

    actual fun start() {
        if (!started) {
            thread {
                try {
                    started = true
                    udpDiscoveryServer = DatagramSocket(DISCOVERY_PORT)
                    udpDiscoveryServer?.reuseAddress = true
                    Napier.i("UDP discovery server is running on port ${udpDiscoveryServer?.localPort}")

                    var receivedData = ByteArray(DISCOVERY_DATA.length)
                    var sendData: ByteArray? = null
                    while (started) {
                        val receivedPacket = DatagramPacket(receivedData, receivedData.size)
                        udpDiscoveryServer?.receive(receivedPacket)

                        val clientIpAddress = receivedPacket.address
                        val clientPort = receivedPacket.port

                        Napier.i("Received data from $clientIpAddress")

                        val data = receivedPacket.data
                        val received = String(data, 0, data.size).trimEnd()

                        if (received == DISCOVERY_DATA) {

                            Napier.i("Sending data to $clientIpAddress:$clientPort")

                            val serverResponse = MapServerResponse(
                                if (mapName.isNotEmpty()) mapName else "MAP_NAME",
                                NetworkUtils.getLocalIpAddress()?.hostAddress ?: ""
                            )
                            sendData = Gson().toJson(serverResponse).toByteArray()

                            val responsePacket =
                                DatagramPacket(sendData, sendData.size, clientIpAddress, clientPort)
                            udpDiscoveryServer?.send(responsePacket)
                            receivedData = ByteArray(DISCOVERY_DATA.length)
                        }
                    }
                } catch (e: IOException) {
                    Napier.i("UDPServer exception - $e")
                } finally {
                    stop()
                }
            }
        }
    }

    actual fun stop() {
        if (started) {
            started = false
            Napier.i("Stopping UDP discovery server...")
            if (udpDiscoveryServer != null) {
                if (udpDiscoveryServer?.isConnected == true) {
                    udpDiscoveryServer?.disconnect()
                }
                udpDiscoveryServer?.close()
                udpDiscoveryServer = null
            }
        }
    }

    actual fun searchForServers(
        onDiscovered: (response: MapServerResponse) -> Unit,
        onDiscoveryFinished: () -> Unit,
        onDiscoveryFailed: (e: Exception) -> Unit
    ) {
        thread {
            udpDiscoveryClient = null
            try {
                udpDiscoveryClient = DatagramSocket(null)
                udpDiscoveryClient?.soTimeout = RESPONSE_TIMEOUT
                udpDiscoveryClient?.reuseAddress = true
                udpDiscoveryClient?.broadcast = true
                udpDiscoveryClient?.bind(InetSocketAddress(UdpServer.DISCOVERY_PORT))

                val sendData = UdpServer.DISCOVERY_DATA.toByteArray()
                val sendPacket = DatagramPacket(
                    sendData, sendData.size, InetAddress.getByName("255.255.255.255"), UdpServer.DISCOVERY_PORT
                )
                udpDiscoveryClient?.send(sendPacket)

                try { // We don't have break in our while loop because we have to accept all the responses from multiple ip module devices
                    while (true) {
                        val receiveBuf = ByteArray(255)
                        val packet = DatagramPacket(receiveBuf, receiveBuf.size)
                        udpDiscoveryClient?.receive(packet)
                        val data = packet.data
                        val devicesIp: InetAddress? = NetworkUtils.getLocalIpAddress()
                        if (packet.address != devicesIp) {
                            Napier.i("Searching for servers - Packet received from: ${packet.address} devicesIp:$devicesIp")
                            val response: MapServerResponse = Gson().fromJson(String(data, 0, data.size).substring(0, String(data, 0, data.size).lastIndexOf("}") + 1), MapServerResponse::class.java)
                            onDiscovered(response)
                        }
                    }
                } catch (e: SocketTimeoutException) {
                    onDiscoveryFinished()
                } catch (e: Exception) {
                    Napier.i("Exception: ${e.message}")
                    onDiscoveryFailed(e)
                }
            } catch (e: IOException) {
                Napier.i("IOException: ${e.message}")
                onDiscoveryFailed(e)
            } finally {
                if (udpDiscoveryClient != null) {
                    if (udpDiscoveryClient?.isConnected == true) {
                        udpDiscoveryClient?.disconnect()
                    }
                    udpDiscoveryClient?.close()
                    udpDiscoveryClient = null
                }
            }
        }
    }

    companion object {
        const val DISCOVERY_PORT = 5003
        const val DISCOVERY_DATA = "MAP_SERVER_DISCOVERY"
        const val RESPONSE_TIMEOUT = 2 * 1000 // X seconds
    }
}
