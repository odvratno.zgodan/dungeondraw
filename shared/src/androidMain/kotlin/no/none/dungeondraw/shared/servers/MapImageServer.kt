package no.none.dungeondraw.shared.servers

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondFile
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.jetty.Jetty
import io.ktor.server.jetty.JettyApplicationEngine
import java.io.File

internal actual class MapImageServer {

    private var server: JettyApplicationEngine? = null
    private var started: Boolean = false
    actual var imagePath = ""
    private val serverImagePath =
        "http://${NetworkUtils.getLocalIpAddress().toString().replace("/", "")}:8080/map_image.jpg"
    private val htmlContent = """
        <!DOCTYPE html>
        <html>
        <head>
        <style>
        body {
          margin: 0;
          padding: 0;
          }
        div.img {
          background-image: url('$serverImagePath');
          background-position: center center;
          background-repeat: no-repeat;
          background-size: contain;
          background-color: #000000;
          width: 100vw;
          height: 100vh;
          -webkit-transition: opacity 1s ease-in-out;
          -moz-transition: opacity 1s ease-in-out;
          -o-transition: opacity 1s ease-in-out;
          transition: opacity 1s ease-in-out;
          }
        </style>
        <script>
            setTimeout(function () {
                location.reload();
            }, 5 * 1000);
        </script>
        </head>
        <body>
            <div class="img"></div>

        </body>
        </html>
    """.trimIndent()

    actual fun start() {
        if (!started) {
            println("Server started... at ${NetworkUtils.getLocalIpAddress().toString().replace("/", "")}")
            started = true
            server = embeddedServer(Jetty, 8080) {
                install(ContentNegotiation) {
                    gson {}
                }
                routing {
                    get("/") {
                        call.respondText(htmlContent, ContentType("text", "html"), HttpStatusCode.OK)
                    }
                    get("/map_image.jpg") {
                        call.respondFile(File(imagePath))
                    }
                }
            }.start(wait = false)
        }
    }

    actual fun stop() {
        if (started) {
            println("Server stopped.")
            started = false
            server?.stop(2000, 5000)
        }
    }
}
