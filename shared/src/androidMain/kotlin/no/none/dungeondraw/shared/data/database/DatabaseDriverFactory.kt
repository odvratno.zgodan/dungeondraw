package no.none.dungeondraw.shared.data.database

import android.content.Context
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import no.none.dungeondraw.shared.db.AppDatabase

lateinit var appContext: Context

actual fun createDriver(): SqlDriver {
    return AndroidSqliteDriver(AppDatabase.Schema, appContext, "test.db")
}
