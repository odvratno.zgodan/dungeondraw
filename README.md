# DungeonDraw(D-KMP)

This repository branch is a port of the DungeonDraw app to Koltin Multiplatform and the UI to Jetpack Compose(Android & Desktop, iOS pending)
It is intended as a playground for evaluating the D-KMP architecture. D-KMP stands for Declarative-KotlinMultiPlaform.

The project is more complex than the usual samples:

* Two way data flow - not only unidirectional data to the UI but also from the UI to the data layer(saving edits into the DB)
* Usage of Compose Canvas for drawing
* File picker and file loading from disk in Desktop implementation
* State and MutableState handling

It has been inspired by Daniele Baroncelli [talk at Droidcon](https://www.droidcon.com/media-detail?video=491032635)
and his series of [posts](https://danielebaroncelli.medium.com/the-future-of-apps-declarative-uis-with-kotlin-multiplatform-d-kmp-part-1-3-c0e1530a5343).

**Developed with: Android Studio Arctic Fox, 2020.3.1 Canary 5**


## Known issues

* Compose preview doesn't work because there are duplicate dependencies when using Compose for multiplatform
* iOS has issues with Ktor
* Issue with creating desktop package, seems that it doesn't package some dependencies. [Jetpack Compose issue](https://github.com/JetBrains/compose-jb/issues/349) and [Ktor issue](https://github.com/cashapp/sqldelight/issues/2151)