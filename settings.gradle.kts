pluginManagement {
    repositories {
        google()
        jcenter()
        gradlePluginPortal()
        mavenCentral()
        // Used for Compose Desktop
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}
rootProject.name = "DungeonDraw"

include(":shared", ":sharedCompose", ":androidApp", ":desktopApp")
