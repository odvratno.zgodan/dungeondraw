package no.none.dungeondraw

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.width
import androidx.compose.material.DrawerValue
import androidx.compose.material.FabPosition
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.rememberDrawerState
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asAndroidBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.unit.dp
import androidx.lifecycle.lifecycleScope
import com.kinandcarta.permissionmanager.permissions.Permission
import com.kinandcarta.permissionmanager.permissions.PermissionManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import no.none.dungeondraw.compose.ui.ComposePlaygroundTheme
import no.none.dungeondraw.compose.ui.MapDetails
import no.none.dungeondraw.compose.ui.MapDrawingControls
import no.none.dungeondraw.compose.ui.MapLists
import no.none.dungeondraw.compose.ui.PaintView
import no.none.dungeondraw.compose.ui.primary
import no.none.dungeondraw.shared.data.database.appContext
import no.none.dungeondraw.shared.data.navigation.MainScreen
import no.none.dungeondraw.shared.data.viewmodel.MapsViewModel
import no.none.dungeondraw.shared.data.viewmodel.NavigationViewModel
import no.none.dungeondraw.shared.data.viewmodel.deleteMapFingerPath
import no.none.dungeondraw.shared.data.viewmodel.deleteMapFingerPaths
import no.none.dungeondraw.shared.data.viewmodel.drawMap
import no.none.dungeondraw.shared.data.viewmodel.editMap
import no.none.dungeondraw.shared.data.viewmodel.goBack
import no.none.dungeondraw.shared.data.viewmodel.loadAllMaps
import no.none.dungeondraw.shared.data.viewmodel.loadMapDetails
import no.none.dungeondraw.shared.data.viewmodel.newMap
import no.none.dungeondraw.shared.data.viewmodel.saveMapDetails
import no.none.dungeondraw.shared.data.viewmodel.saveMapFingerPath
import no.none.dungeondraw.shared.data.viewmodel.setRenderedImagePath
import no.none.dungeondraw.shared.data.viewmodel.startServerDiscovery
import no.none.dungeondraw.shared.data.viewmodel.startUDPServer
import no.none.dungeondraw.shared.data.viewmodel.stopServerDiscovery
import no.none.dungeondraw.shared.data.viewmodel.stopUDPServer
import no.none.dungeondraw.shared.data.vo.FingerPath
import no.none.dungeondraw.shared.data.vo.Map
import no.none.dungeondraw.shared.data.vo.Result
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

// import androidx.compose.ui.tooling.preview.Preview

class MainActivity : AppCompatActivity() {

    private val mapsViewModel by lazy {
        MapsViewModel()
    }

    private val navigationViewModel by lazy {
        NavigationViewModel()
    }

    private val permissionManager = PermissionManager.from(this)

    private val mapImagePath: MutableState<String> = mutableStateOf("")

    private val storagePermissionGranted: MutableState<Boolean> = mutableStateOf(false)

    private val getImageUri =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            // Handle the returned Uri
            val bitmapImage = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
            lifecycleScope.launch(Dispatchers.IO) {
                val imagePath =
                    saveImageToInternalStorage(bitmapImage, Clock.System.now().toString())
                mapImagePath.value = imagePath
            }
        }

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val isTablet = resources.getBoolean(R.bool.isTablet)
        setContent {
            ComposePlaygroundTheme(isDarkTheme = isSystemInDarkTheme()) {
                if (!isTablet) {
                    SmartphoneUI(navigationViewModel = navigationViewModel, mapsViewModel = mapsViewModel)
                } else {
                    TabletUI(navigationViewModel = navigationViewModel, mapsViewModel = mapsViewModel)
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    @Composable
    fun SmartphoneUI(navigationViewModel: NavigationViewModel, mapsViewModel: MapsViewModel) {
        val navigationState = navigationViewModel.stateFlow.collectAsState()

        when (navigationState.value.currentScreen) {
            MainScreen.LIST -> {
                ListScreen(
                    navigationViewModel,
                    mapsViewModel
                )
                mapImagePath.value = ""
            }
            MainScreen.DRAW -> MapDrawScreen(
                navigationViewModel,
                mapsViewModel
            )
            MainScreen.NEW -> {
                checkStoragePermission()
                MapDetailsScreen(
                    navigationViewModel,
                    mapsViewModel,
                    mapImagePath
                ) {
                    handleLoadImageButtonClick()
                }
            }
            MainScreen.EDIT -> {
                checkStoragePermission()
                MapDetailsScreen(
                    navigationViewModel,
                    mapsViewModel,
                    mapImagePath
                ) {
                    handleLoadImageButtonClick()
                }
            }
        }
        if (navigationState.value.currentScreen == MainScreen.LIST) {
            mapsViewModel.startServerDiscovery()
        } else {
            mapsViewModel.stopServerDiscovery()
        }
    }

    @ExperimentalCoroutinesApi
    @Composable
    fun TabletUI(navigationViewModel: NavigationViewModel, mapsViewModel: MapsViewModel) {
        val navigationState = navigationViewModel.stateFlow.collectAsState()

        when (navigationState.value.currentScreen) {
            MainScreen.LIST, MainScreen.NEW, MainScreen.EDIT -> {
                checkStoragePermission()
                MapDetailsTabletScreen(
                    navigationViewModel,
                    mapsViewModel,
                    mapImagePath
                ) {
                    handleLoadImageButtonClick()
                }
            }
            MainScreen.DRAW -> MapDrawScreen(
                navigationViewModel,
                mapsViewModel
            )
        }

        if (navigationState.value.currentScreen == MainScreen.LIST) {
            mapsViewModel.startServerDiscovery()
        } else {
            mapsViewModel.stopServerDiscovery()
        }
    }

    override fun onBackPressed() {
        if (navigationViewModel.stateFlow.value.screens.size > 1) {
            navigationViewModel.goBack()
            return
        }
        super.onBackPressed()
    }

    private fun handleLoadImageButtonClick() {
        if (!storagePermissionGranted.value) {
            requestStoragePermission()
        } else {
            loadImage()
        }
    }

    private fun checkStoragePermission() {
        permissionManager
            .checkPermission { granted: Boolean ->
                println("permissions are granted:$granted")
                storagePermissionGranted.value = granted
            }
    }

    private fun requestStoragePermission() {
        permissionManager
            .request(Permission.StorageReadOnly)
            .rationale("We need permission to load images")
            .checkPermission { granted: Boolean ->
                storagePermissionGranted.value = granted
            }
    }

    private fun loadImage() {
        getImageUri.launch("image/*")
    }

    private fun saveImageToInternalStorage(bitmap: Bitmap, createdAt: String): String {
        val path: String = this.filesDir.absolutePath
        val file = File(path, "map_image_$createdAt.jpg")
        val fOut = FileOutputStream(file)

        bitmap.compress(Bitmap.CompressFormat.JPEG, 95, fOut)
        fOut.flush() // Not really required
        fOut.close() // do not forget to close the stream
        return file.absolutePath
    }
}

@ExperimentalCoroutinesApi
@Composable
fun ListScreen(navigationViewModel: NavigationViewModel, mapsViewModel: MapsViewModel) {
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
    val navigationState = navigationViewModel.stateFlow.collectAsState()

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = { Text("TopAppBar", color = Color.White) },
                backgroundColor = primary
            )
        },
        floatingActionButtonPosition = FabPosition.End,
        floatingActionButton = {
            if (navigationState.value.currentScreen == MainScreen.LIST) {
                FloatingActionButton(
                    onClick = {
                        navigationViewModel.newMap()
                    }
                ) {
                    Icon(imageVector = Icons.Default.Add, contentDescription = null)
                }
            }
        },
        /*
        // THIS IS HOW A DRAWER IS IMPLEMENTED. EASY PEASY!
        drawerContent = { Text(text = "drawerContent") },
        */
        content = {
            MapLists(
                mapsViewModel,
                onSelected = {
                    mapsViewModel.loadMapDetails(it)
                    navigationViewModel.drawMap()
                }
            )
        }
    )

    mapsViewModel.loadAllMaps()
    mapsViewModel.stopUDPServer()
}

@ExperimentalCoroutinesApi
@Composable
fun MapDrawScreen(navigationViewModel: NavigationViewModel, mapsViewModel: MapsViewModel) {
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
    val map = mapsViewModel.stateFlow.collectAsState().value.map
    val serverAddress = mapsViewModel.stateFlow.collectAsState().value.serverAddress
    val fingerPathsResult = mapsViewModel.stateFlow.collectAsState().value.mapFingerPaths
    val undoFingerPaths = remember { mutableStateOf(listOf<FingerPath>()) }
    var mapImage: ImageBitmap? = null
    if (map is Result.Success) {
        val mapImagePath = remember { mutableStateOf(map.data.imagePath) }
        if (mapImagePath.value.isNotEmpty()) {
            mapImage = BitmapFactory.decodeFile(mapImagePath.value).asImageBitmap()
        }
    }

    var mapName = ""
    if (map is Result.Success) {
        mapName = map.data.name
    }

    var serverAddressString = ""
    if (serverAddress is Result.Success) {
        serverAddressString = "http://${serverAddress.data.removePrefix("/")}:8080"
    }

    val showMasked = remember { mutableStateOf(false) }
    val brushSize = remember { mutableStateOf(18f) }
    val brushSliderVisible = remember { mutableStateOf(false) }
    val defaultPathStyle = remember { mutableStateOf(FingerPath.Style.FILL) }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = { Text("$mapName at $serverAddressString", color = Color.White) },
                backgroundColor = primary,
                actions = {
                    IconButton(
                        onClick = {
                            navigationViewModel.editMap()
                        }
                    ) {
                        Icon(Icons.Default.Edit, contentDescription = null)
                    }
                }
            )
        },
        content = {
            Column {
                if (map is Result.Success && fingerPathsResult is Result.Success) {
                    MapDrawingControls(
                        brushSize = brushSize,
                        showMasked = showMasked,
                        defaultPathStyle = defaultPathStyle,
                        brushSliderVisible = brushSliderVisible,
                        fingerPaths = fingerPathsResult.data,
                        undoFingerPaths = undoFingerPaths,
                        undoOnClick = {
                            val undoOriginal = undoFingerPaths.value.toMutableList()
                            fingerPathsResult.data.last().apply {
                                undoOriginal.add(this)
                                undoFingerPaths.value = undoOriginal
                                println(">>> MainActivity.undoOnClick() path.id:${this.id}")
                                mapsViewModel.deleteMapFingerPath(this)
                            }
                        },
                        redoOnClick = {
                            val undoOriginal = undoFingerPaths.value.toMutableList()
                            undoOriginal.removeLast().apply {
                                this.id = 0L
                                undoFingerPaths.value = undoOriginal
                                println(">>> MainActivity.redoOnClick() path.id:${this.id}")
                                mapsViewModel.saveMapFingerPath(this)
                            }
                        },
                        clearOnClick = {
                            println(">>> MainActivity.clearOnClick()")
                            undoFingerPaths.value = listOf()
                            mapsViewModel.deleteMapFingerPaths(map.data.id)
                        }
                    )
                    PaintView(
                        map.data.id,
                        fingerPathsResult.data.toMutableList(),
                        undoFingerPaths,
                        mapImage,
                        brushSize = brushSize,
                        showMasked = showMasked,
                        defaultPathStyle = defaultPathStyle,
                        onFingerDraw = { path ->
                            println(">>> MainActivity.onFingerDraw() path.id:${path.id}")
                            mapsViewModel.saveMapFingerPath(path)
                        },
                        onImageDraw = { image ->
                            println(">>> MainActivity.onImageDraw()")
                            val path = saveImage(context = appContext, image.asAndroidBitmap())
                            mapsViewModel.setRenderedImagePath(path)
                        }
                    )
                }
            }
        }
    )

    mapsViewModel.startUDPServer()
}

@ExperimentalCoroutinesApi
@Composable
fun MapDetailsScreen(
    navigationViewModel: NavigationViewModel,
    mapsViewModel: MapsViewModel,
    mapImagePath: MutableState<String>,
    imageButtonOnClick: () -> Unit
) {
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
    val map = if (mapsViewModel.stateFlow.collectAsState().value.map is Result.Success) (mapsViewModel.stateFlow.collectAsState().value.map as Result.Success).data else Map()
    val mapName = remember { mutableStateOf(map.name) }
    var mapImage: ImageBitmap? = null

    if (map.id != 0L && map.imagePath.isNotEmpty()) {
        mapImagePath.value = map.imagePath
    }

    if (mapImagePath.value.isNotEmpty()) {
        mapImage = BitmapFactory.decodeFile(mapImagePath.value).asImageBitmap()
    }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = { Text("TopAppBar", color = Color.White) },
                backgroundColor = primary,
                actions = {
                    IconButton(
                        onClick = {
                            map.name = mapName.value
                            map.imagePath = mapImagePath.value
                            mapImagePath.value = ""
                            mapsViewModel.saveMapDetails(map)
                            navigationViewModel.goBack()
                        }
                    ) {
                        Icon(Icons.Default.Done, contentDescription = null)
                    }
                }
            )
        },
        content = {
            MapDetails(
                mapName = mapName, mapImage = mapImage,
                imageButtonOnClick = {
                    imageButtonOnClick.invoke()
                }
            )
        }
    )
    mapsViewModel.stopUDPServer()
}

@ExperimentalCoroutinesApi
@Composable
fun MapDetailsTabletScreen(
    navigationViewModel: NavigationViewModel,
    mapsViewModel: MapsViewModel,
    mapImagePath: MutableState<String>,
    imageButtonOnClick: () -> Unit
) {
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
    val navigationState = navigationViewModel.stateFlow.collectAsState()

    val map = if (mapsViewModel.stateFlow.collectAsState().value.map is Result.Success) (mapsViewModel.stateFlow.collectAsState().value.map as Result.Success).data else Map()
    val mapName = remember { mutableStateOf(map.name) }
    var mapImage: ImageBitmap? = null

    var topAppBarTitle = ""
    if (navigationState.value.currentScreen == MainScreen.LIST) {
        topAppBarTitle = "DungeonDraw"
    } else if (mapName.value.isNotEmpty()) {
        topAppBarTitle = mapName.value
    }

    if (map.id != 0L && map.imagePath.isNotEmpty()) {
        mapImagePath.value = map.imagePath
    }

    if (mapImagePath.value.isNotEmpty()) {
        mapImage = BitmapFactory.decodeFile(mapImagePath.value).asImageBitmap()
    }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = { Text(topAppBarTitle, color = Color.White) },
                backgroundColor = primary,
                actions = {
                    when (navigationState.value.currentScreen) {
                        MainScreen.LIST -> {
                            IconButton(
                                onClick = {
                                    navigationViewModel.newMap()
                                }
                            ) {
                                Icon(Icons.Default.Add, contentDescription = null)
                            }
                        }
                        MainScreen.NEW, MainScreen.EDIT -> {
                            if (map.id != 0L) {
                                IconButton(
                                    onClick = {
                                        mapsViewModel.loadMapDetails(map)
                                        navigationViewModel.drawMap()
                                    }
                                ) {
                                    Icon(Icons.Default.Edit, contentDescription = null)
                                }
                            }
                            IconButton(
                                onClick = {
                                    map.name = mapName.value
                                    map.imagePath = mapImagePath.value
                                    mapImagePath.value = ""
                                    mapsViewModel.saveMapDetails(map)
                                    navigationViewModel.goBack()
                                }
                            ) {
                                Icon(Icons.Default.Done, contentDescription = null)
                            }
                        }
                    }
                }
            )
        },
        content = {
            Row(Modifier.fillMaxSize()) {

                Box(
                    Modifier
                        .width(320.dp)
                        .fillMaxHeight()
                ) {
                    MapLists(
                        mapsViewModel,
                        onSelected = {
                            mapsViewModel.loadMapDetails(it)
                            navigationViewModel.editMap()
                        }
                    )
                }

                Spacer(
                    modifier = Modifier
                        .width(1.dp)
                        .fillMaxHeight()
                        .background(Color.LightGray)
                )
                when (navigationState.value.currentScreen) {
                    MainScreen.NEW, MainScreen.EDIT -> Box(Modifier.fillMaxHeight()) {
                        MapDetails(
                            mapName = mapName, mapImage = mapImage,
                            imageButtonOnClick = {
                                imageButtonOnClick.invoke()
                            }
                        )
                    }
                }
            }
        }
    )
    if (navigationState.value.currentScreen == MainScreen.LIST) {
        mapImagePath.value = ""
        mapsViewModel.loadAllMaps()
    }
    mapsViewModel.stopUDPServer()
}

fun saveImage(context: Context, bitmap: Bitmap): String {
    val file = File(context.filesDir.absolutePath, "map_image.jpg")
    val fOut = FileOutputStream(file)

    // Resize image to fit inside 1280x720
    /*val maxWidth = 1280
    val maxHeight = 720
    val scale = min(maxWidth.toFloat() / bitmap.width, maxHeight.toFloat() / bitmap.height)
    val matrix = Matrix()
    matrix.postScale(scale, scale)

    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)*/

    bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut)
    fOut.flush() // Not really required
    fOut.close() // do not forget to close the stream
    return file.absolutePath
}

fun Bitmap.convertToByteArray(): ByteArray {
    // return bitmap's pixels
    val stream = ByteArrayOutputStream()
    this.compress(Bitmap.CompressFormat.PNG, 80, stream)
    return stream.toByteArray()
}

/*@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposePlaygroundTheme {
    }
}*/
