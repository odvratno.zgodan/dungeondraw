package no.none.dungeondraw

import android.app.Application
import com.github.aakira.napier.DebugAntilog
import com.github.aakira.napier.Napier
import no.none.dungeondraw.shared.data.database.appContext

class DKMPApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Napier.base(DebugAntilog())
        appContext = this
    }
}
