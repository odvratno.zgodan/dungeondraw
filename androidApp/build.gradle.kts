plugins {
    id("org.jetbrains.compose") version Versions.compose
    id("com.android.application")
    id("io.gitlab.arturbosch.detekt")
    kotlin("android")
}

val composeVersion = "1.0.0-beta01"
dependencies {

    implementation(project(":shared"))
    implementation(project(":sharedCompose"))

    implementation("org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}")

    implementation(Androidx.ktxCore)
    implementation(Androidx.appCompat)
    implementation(Androidx.activity)
    implementation(Material.material)

    // Lifecycle components
    implementation(Lifecycle.runtime)
    implementation(Lifecycle.viewmodel)
    implementation(Lifecycle.extensions)

    implementation(Androidx.activityCompose)

    // Kotlin components
    api(Coroutines.coroutinesCore)
    api(Coroutines.coroutinesAndroid)

    implementation(Kotlinx.datetime)

    implementation(Logging.napier)

    // Required for Kotlinx-datetime if targeting API<26
    coreLibraryDesugaring(Kotlinx.datetimeDesugarJdk)
}

android {
    compileSdkVersion(AndroidSdk.compile)
    defaultConfig {
        applicationId = "no.none.dungeondraw"
        minSdkVersion(AndroidSdk.min)
        targetSdkVersion(AndroidSdk.compile)
        versionCode = 1
        versionName = "1.0"
        // Required for Kotlinx-datetime if targeting API<26
        multiDexEnabled = true
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }

    buildFeatures {
        compose = true
    }

    compileOptions {
        // Required for Kotlinx-datetime if targeting API<26
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility(JavaVersion.VERSION_1_8)
        targetCompatibility(JavaVersion.VERSION_1_8)
    }

    kotlinOptions {
        jvmTarget = "1.8"
        useIR = true
    }

    composeOptions {
        kotlinCompilerVersion = Versions.kotlin
        kotlinCompilerExtensionVersion = composeVersion
    }

    packagingOptions {
        // This is needed for the Ktor-server, otherwise it doesn't work
        exclude("META-INF/*")
    }

    detekt {
        config = files("$rootDir/default-detekt-config.yml")
    }
}
