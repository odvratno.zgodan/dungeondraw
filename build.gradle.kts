plugins {
    `kotlin-dsl`
}

buildscript {
    repositories {
        jcenter()
        google()
        mavenCentral()
        maven("https://plugins.gradle.org/m2/")
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}")
        classpath("org.jetbrains.kotlin:kotlin-serialization:${Versions.kotlinSerializationPlugin}")
        classpath("com.android.tools.build:gradle:7.1.0-alpha01")
        classpath("com.squareup.sqldelight:gradle-plugin:${Versions.sqlDelight}")
        classpath("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:${Versions.detekt}")
        classpath("org.jlleitschuh.gradle:ktlint-gradle:${Versions.ktlintGradle}")
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        mavenCentral()
        maven(url = "https://kotlin.bintray.com/kotlinx/")
        maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
    }
}

subprojects {
    apply(plugin = "org.jlleitschuh.gradle.ktlint") // Version should be inherited from parent

    // Optionally configure plugin
    configure<org.jlleitschuh.gradle.ktlint.KtlintExtension> {
        debug.set(true)
        /*verbose.set(true)*/
        val rulesToDisable = listOf("no-wildcard-imports")
        disabledRules.set(rulesToDisable)
        filter {
            exclude("**/generated/**")
            include("**/kotlin/**")
        }
    }
//    System.setProperty("org.sqlite.lib.path", "libSigningHack/sqlite-jdbc-3.34.0.jar");
}

apply(plugin = "io.gitlab.arturbosch.detekt")
apply(plugin = "org.jlleitschuh.gradle.ktlint")
