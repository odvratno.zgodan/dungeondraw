package no.none.dungeondraw.compose.ui

// This is needed to use following pattern
// [variable by remember]
// This is needed to use following pattern
// [variable by remember]
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Slider
import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Brush
import androidx.compose.material.icons.filled.Circle
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.LinearScale
import androidx.compose.material.icons.filled.Redo
import androidx.compose.material.icons.filled.Undo
import androidx.compose.material.icons.filled.Warning
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Canvas
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.ImageBitmapConfig
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.PaintingStyle
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.StrokeJoin
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import kotlinx.coroutines.ExperimentalCoroutinesApi
import no.none.dungeondraw.shared.data.viewmodel.MapsViewModel
import no.none.dungeondraw.shared.data.vo.DrawPath
import no.none.dungeondraw.shared.data.vo.FingerPath
import no.none.dungeondraw.shared.data.vo.Map
import no.none.dungeondraw.shared.data.vo.Result
import no.none.dungeondraw.shared.servers.MapServerResponse
import kotlin.math.min

@ExperimentalCoroutinesApi
@Composable
fun MapLists(mapsViewModel: MapsViewModel, onSelected: (map: Map) -> Unit?) {
    val scrollState = rememberScrollState()
    val appState = mapsViewModel.stateFlow.collectAsState()
    val mapsResult = appState.value.maps
    val remoteMapsResult = appState.value.remoteMaps

    var selectedTab by remember { mutableStateOf(0) }

    val titles = listOf("My maps", "Remote maps")

    Column(modifier = Modifier.verticalScroll(scrollState)) {
        TabRow(
            selectedTabIndex = selectedTab,
            indicator = { },
            divider = { }
        ) {
            titles.forEachIndexed { index, title ->
                val selected = index == selectedTab

                val textModifier = Modifier.padding(vertical = 8.dp, horizontal = 16.dp)
                Tab(
                    selected = selected,
                    onClick = {
                        selectedTab = index
                    }
                ) {
                    Text(
                        modifier = textModifier,
                        text = title.toUpperCase()
                    )
                }
            }
        }

        when (selectedTab) {
            0 -> {
                LocalMapsList(modifier = Modifier.padding(16.dp), maps = mapsResult, onSelected = onSelected)
            }
            1 -> {
                RemoteMapsList(
                    modifier = Modifier.padding(16.dp), maps = remoteMapsResult,
                    onSelected = {
                        println("Selected remote map with name: ${it.name} and IP address: ${it.ipAddress}")
                    }
                )
            }
        }
    }
}

@Composable
fun LocalMapsList(modifier: Modifier = Modifier, maps: Result<List<Map>>, onSelected: (map: Map) -> Unit?) {
    Box(modifier = modifier) {
        if (maps is Result.Loading) {
            Text(
                text = "Loading maps...",
                color = MaterialTheme.colors.primary,
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
                style = TextStyle(
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold
                )
            )
        }
        if (maps is Result.Success) {
            Column {
                for (item in maps.data) {
                    MapListItem(
                        Modifier
                            .fillMaxWidth()
                            .padding(8.dp, 4.dp)
                            .clickable(onClick = { onSelected.invoke(item) }),
                        map = item
                    )
                }
            }
        }
    }
}

@Composable
fun RemoteMapsList(modifier: Modifier = Modifier, maps: Result<List<MapServerResponse>>, onSelected: (map: MapServerResponse) -> Unit?) {
    Box(modifier = modifier) {
        if (maps is Result.Loading) {
            Text(
                text = "Searching for remote maps...",
                color = MaterialTheme.colors.primary,
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
                style = TextStyle(
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold
                )
            )
        }
        if (maps is Result.Success) {
            Column {
                for (item in maps.data) {
                    RemoteMapListItem(
                        Modifier
                            .fillMaxWidth()
                            .padding(8.dp, 4.dp)
                            .clickable(
                                onClick = {
                                    onSelected.invoke(item)
                                }
                            ),
                        remoteMap = item
                    )
                }
            }
        }
    }
}

@Composable
fun MapListItem(modifier: Modifier = Modifier, map: Map) {
    Card(
        modifier = modifier, elevation = 2.dp
    ) {
        Row(
            modifier = Modifier
                .padding(8.dp)
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            if (map.imagePath.isNotEmpty()) {
                // TODO: Replace with a multiplatform image loading library
                /*CoilImage(
                    data = image.media.url,
                    modifier = Modifier.preferredSize(60.dp),
                    requestBuilder = {
                        transformations(CircleCropTransformation())
                    })*/
            } else {
                Spacer(modifier = Modifier.size(60.dp))
            }
            Column(modifier = Modifier.padding(8.dp)) {
                Text(
                    text = "Title: ${map.name}",
                    fontWeight = FontWeight.Bold,
                )
            }
        }
    }
}

@Composable
fun RemoteMapListItem(modifier: Modifier = Modifier, remoteMap: MapServerResponse) {
    Card(
        modifier = modifier, elevation = 2.dp
    ) {
        Row(
            modifier = Modifier
                .padding(8.dp)
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(modifier = Modifier.padding(8.dp)) {
                Text(
                    text = "Title: ${remoteMap.name}",
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colors.primary
                )
            }
        }
    }
}

@ExperimentalCoroutinesApi
@Composable
fun MapDetails(
    mapName: MutableState<String>,
    mapImage: ImageBitmap?,
    imageButtonOnClick: () -> Unit
) {

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TextField(
            modifier = Modifier.padding(8.dp),
            value = mapName.value,
            onValueChange = {
                mapName.value = it
            },
            label = { Text("Dungeon name") },
            singleLine = true
        )

        Button(
            modifier = Modifier.padding(8.dp),
            onClick = {
                imageButtonOnClick()
            }
        ) {
            Text("Select image")
        }

        mapImage?.let { Image(it, contentDescription = null, modifier = Modifier.padding(8.dp)) }
    }
}

const val DEFAULT_ALPHA = 0.5f
val DEFAULT_COLOR = Color.Red

@ExperimentalCoroutinesApi
@Composable
fun PaintView(
    mapId: Long,
    fingerPaths: List<FingerPath>,
    undoFingerPaths: MutableState<List<FingerPath>>,
    mapImage: ImageBitmap?,
    brushSize: MutableState<Float>,
    showMasked: MutableState<Boolean>,
    defaultPathStyle: MutableState<FingerPath.Style>,
    onFingerDraw: (path: FingerPath) -> Unit,
    onImageDraw: (image: ImageBitmap) -> Unit,
) {
    val DEFAULT_COLOR = Color.Red

    val tempPoints = remember { mutableStateOf(listOf<Pair<Float, Float>>()) }

    var overlayImageBitmap = remember { ImageBitmap(1, 1) }
    var maskedImageBitmap = remember { ImageBitmap(1, 1) }

    // var scale = remember { mutableStateOf(1.5f) }
    // var translation = remember { mutableStateOf(Offset(0f, 0f)) }

    var scale by remember { mutableStateOf(1f) }
    var offset by remember { mutableStateOf(Offset.Zero) }

    var size by remember { mutableStateOf(Size.Unspecified) }

    mapImage?.let {
        if (size != Size.Unspecified) {
            scale = min(size.width / it.width, size.height / it.height)
            offset = Offset(
                (size.width - it.width) * scale * 0.5f,
                (size.height - it.height) * scale * 0.5f
            )
        }
        overlayImageBitmap = ImageBitmap(it.width, it.height)
        val overlayCanvas = Canvas(overlayImageBitmap)
        overlayCanvas.drawImage(it, Offset(0f, 0f), Paint())
        for (fp in fingerPaths) {
            overlayCanvas.drawPath(fp)
        }

        maskedImageBitmap = ImageBitmap(it.width, it.height, config = ImageBitmapConfig.Argb8888)
        val maskedCanvas = Canvas(maskedImageBitmap)
        for (fp in fingerPaths) {
            maskedCanvas.drawPath(fp, drawAsMask = true)
        }
        val paint = Paint()
        paint.blendMode = BlendMode.SrcAtop
        maskedCanvas.drawImage(it, Offset(0f, 0f), paint)

        onImageDraw(maskedImageBitmap)
    }

    Column {
        Canvas(
            modifier = Modifier.zIndex(0f).fillMaxSize().padding(16.dp)
                .graphicsLayer(scaleX = scale, scaleY = scale, translationX = offset.x, translationY = offset.y)
                .pointerInput("") {
                    detectTouchEvents(
                        onTouchDown = { change ->
                            undoFingerPaths.value = listOf()

                            val tempOriginal = tempPoints.value.toMutableList()
                            tempOriginal.add(Pair(change.position.x, change.position.y))
                            tempPoints.value = tempOriginal
                        },
                        onTouchUp = { change ->
                            val tempOriginal = tempPoints.value.toMutableList()
                            tempOriginal.add(Pair(change.position.x, change.position.y))

                            FingerPath(
                                mapId = mapId,
                                color = DEFAULT_COLOR.toArgb(),
                                x = 0f,
                                y = 0f,
                                strokeWidth = brushSize.value,
                                drawPath = DrawPath(tempOriginal.toMutableList()),
                                paintStyle = defaultPathStyle.value
                            ).apply {
                                onFingerDraw(this)
                            }

                            tempPoints.value = listOf()
                        },
                        onTouchMove = { change ->
                            val tempOriginal = tempPoints.value.toMutableList()
                            tempOriginal.add(Pair(change.position.x, change.position.y))
                            tempPoints.value = tempOriginal
                        }
                    )
                },
            onDraw = {
                size = this.size
                drawImage(if (showMasked.value) { maskedImageBitmap } else { overlayImageBitmap })
                drawIntoCanvas {
                    it.drawPath(tempPoints.value, brushSize.value, defaultPathStyle.value)
                }
            }
        )
    }
}

@Composable
fun MapDrawingControls(
    brushSize: MutableState<Float>,
    showMasked: MutableState<Boolean>,
    defaultPathStyle: MutableState<FingerPath.Style>,
    brushSliderVisible: MutableState<Boolean>,
    fingerPaths: List<FingerPath>,
    undoFingerPaths: MutableState<List<FingerPath>>,
    undoOnClick: () -> Unit,
    redoOnClick: () -> Unit,
    clearOnClick: () -> Unit
) {
    val iconModifier = Modifier.size(16.dp)
    Row(modifier = Modifier.zIndex(1f).fillMaxWidth(), horizontalArrangement = Arrangement.End) {
        //region buttons code region
        Button(
            onClick = {
                showMasked.value = !showMasked.value
            },
            shape = CircleShape,
            modifier = Modifier.padding(8.dp).sizeIn(4.dp, 4.dp),
            contentPadding = PaddingValues(6.dp)
        ) {
            val icon = Icons.Filled.Warning
            Icon(icon, "", modifier = iconModifier)
        }

        Button(
            onClick = {
                if (defaultPathStyle.value == FingerPath.Style.FILL) {
                    defaultPathStyle.value = FingerPath.Style.STROKE
                } else {
                    defaultPathStyle.value = FingerPath.Style.FILL
                }
            },
            shape = CircleShape,
            modifier = Modifier.padding(8.dp).sizeIn(4.dp, 4.dp),
            contentPadding = PaddingValues(6.dp)
        ) {
            val icon = if (defaultPathStyle.value == FingerPath.Style.FILL) {
                Icons.Filled.Brush
//                    FontAwesomeIcons.Solid.PaintBrush
            } else {
                Icons.Filled.Circle
//                    FontAwesomeIcons.Solid.DrawPolygon
            }
            Icon(icon, "", modifier = iconModifier)
        }

        Button(
            onClick = {
                brushSliderVisible.value = !brushSliderVisible.value
            },
            shape = CircleShape,
            modifier = Modifier.padding(8.dp).sizeIn(4.dp, 4.dp),
            contentPadding = PaddingValues(6.dp)
        ) {
            Icon(Icons.Filled.LinearScale, "", modifier = iconModifier)
//                Icon(FontAwesomeIcons.Solid.SlidersH, "", modifier = iconModifier)
        }

        Button(
            onClick = {
                clearOnClick()
            },
            enabled = fingerPaths.isNotEmpty(),
            shape = CircleShape,
            modifier = Modifier.padding(8.dp).sizeIn(4.dp, 4.dp),
            contentPadding = PaddingValues(6.dp)
        ) {
            Icon(Icons.Filled.Clear, "", modifier = iconModifier)
        }
        //endregion
        Button(
            onClick = {
                undoOnClick()
            },
            enabled = fingerPaths.isNotEmpty(),
            shape = CircleShape,
            modifier = Modifier.padding(8.dp).sizeIn(4.dp, 4.dp),
            contentPadding = PaddingValues(6.dp)
        ) {
            Icon(Icons.Filled.Undo, "", modifier = iconModifier)
//                Icon(FontAwesomeIcons.Solid.Undo, "", modifier = iconModifier)
        }

        Button(
            onClick = {
                redoOnClick()
            },
            enabled = undoFingerPaths.value.isNotEmpty(),
            shape = CircleShape,
            modifier = Modifier.padding(8.dp).sizeIn(4.dp, 4.dp),
            contentPadding = PaddingValues(6.dp)
        ) {
            Icon(Icons.Filled.Redo, "", modifier = iconModifier)
//                Icon(FontAwesomeIcons.Solid.Redo, "", modifier = iconModifier)
        }
    }
    if (brushSliderVisible.value) {
        Slider(
            value = brushSize.value,
            valueRange = 8f..64f,
            onValueChange = { brushSize.value = it }
        )
    }
}

@Composable
fun MapDrawerDrawingControls(
    brushSize: MutableState<Float>,
    showMasked: MutableState<Boolean>,
    defaultPathStyle: MutableState<FingerPath.Style>,
    onNavigateBackClick: () -> Unit,
    drawerChanged: (Boolean) -> Unit
) {
    Column(modifier = Modifier.fillMaxSize().wrapContentWidth().padding(16.dp)) {
        Button(
            onClick = {
                drawerChanged(false)
            },
            shape = CircleShape,
            contentPadding = PaddingValues(8.dp),
            modifier = Modifier.padding(8.dp).sizeIn(16.dp, 16.dp),
        ) {
            Icon(Icons.Default.Close, contentDescription = null)
        }
        Spacer(Modifier.height(32.dp))
        Button(
            onClick = {
                showMasked.value = !showMasked.value
            }
        ) {
            Text(
                text = if (showMasked.value) "Masked mode" else "Unmasked mode",
                style = MaterialTheme.typography.h6
            )
        }
        Spacer(Modifier.height(16.dp))
        Button(
            onClick = {
                if (defaultPathStyle.value == FingerPath.Style.FILL) {
                    defaultPathStyle.value = FingerPath.Style.STROKE
                } else {
                    defaultPathStyle.value = FingerPath.Style.FILL
                }
            }
        ) {
            Text(
                text = if (defaultPathStyle.value == FingerPath.Style.STROKE) "Line drawing mode" else "Polygon drawing mode",
                style = MaterialTheme.typography.h6
            )
        }
        Spacer(Modifier.height(16.dp))
        Row(verticalAlignment = Alignment.CenterVertically) {
            Text("Line thickness", style = MaterialTheme.typography.h6)
            Slider(
                value = brushSize.value,
                valueRange = 8f..64f,
                onValueChange = { brushSize.value = it }
            )
        }
        Spacer(Modifier.height(16.dp))
        Button(
            onClick = {
                println("undo")
            }
        ) {
            Text("Undo", style = MaterialTheme.typography.h6)
        }
        Spacer(Modifier.height(16.dp))
        Button(
            onClick = {
                println("redo")
            }
        ) {
            Text("Redo", style = MaterialTheme.typography.h6)
        }
        Spacer(Modifier.height(16.dp))
        Button(
            onClick = {
                println("clear")
            }
        ) {
            Text("Clear", style = MaterialTheme.typography.h6)
        }
        Spacer(Modifier.height(32.dp))
        Button(
            onClick = {
                onNavigateBackClick()
            }
        ) {
            Text("Back to list", style = MaterialTheme.typography.h6)
        }
    }
}

fun Canvas.drawPath(path: FingerPath, drawAsMask: Boolean = false) {
    val paint = Paint()
    paint.color = Color(path.color)
    if (!drawAsMask) {
        paint.alpha = DEFAULT_ALPHA
    }
    paint.strokeWidth = path.strokeWidth
    paint.strokeJoin = StrokeJoin.Round
    paint.strokeCap = StrokeCap.Round
    when (path.paintStyle) {
        FingerPath.Style.FILL -> paint.style = PaintingStyle.Fill
        FingerPath.Style.STROKE -> paint.style = PaintingStyle.Stroke
    }
    this.drawPath(path.drawPath.points.toPath(), paint)
}

fun Canvas.drawPath(path: List<Pair<Float, Float>>, strokeWidth: Float, style: FingerPath.Style) {
    val paint = Paint()
    paint.color = DEFAULT_COLOR
    paint.alpha = DEFAULT_ALPHA
    paint.strokeWidth = strokeWidth
    paint.strokeJoin = StrokeJoin.Round
    paint.strokeCap = StrokeCap.Round
    when (style) {
        FingerPath.Style.FILL -> paint.style = PaintingStyle.Fill
        FingerPath.Style.STROKE -> paint.style = PaintingStyle.Stroke
    }
    this.drawPath(path.toPath(), paint)
}

fun List<Pair<Float, Float>>.toPath(): Path {
    val path = Path()
    if (size > 1) {
        path.moveTo(first().first, first().second)
        for (point in subList(1, size - 1)) {
            path.lineTo(point.first, point.second)
        }
    }

    return path
}

fun List<Pair<Float, Float>>.toOffsetList(): List<Offset> {
    val list = mutableListOf<Offset>()
    for (point in this) {
        list.add(Offset(point.first, point.second))
    }
    return list
}
