package no.none.dungeondraw.compose.ui

import androidx.compose.ui.graphics.Color

val primary = Color(0xFF6200EE)
val primaryDark = Color(0xFF3700B3)
val accent = Color(0xFF03DAC5)
val backgroundColor = Color(0xFF121212)

val purple200 = Color(0xFFBB86FC)
val purple500 = Color(0xFF6200EE)
val purple700 = Color(0xFF3700B3)
val teal200 = Color(0xFF03DAC5)
