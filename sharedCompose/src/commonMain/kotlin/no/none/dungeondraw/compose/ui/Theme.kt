package no.none.dungeondraw.compose.ui

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
    /*primary = purple200,
    primaryVariant = purple700,
    secondary = teal200,
    background = backgroundColor*/
)

private val LightColorPalette = lightColors(
    /*primary = purple500,
    primaryVariant = purple700,
    secondary = teal200,
    background = backgroundColor*/

//      Other default colors to override
    /*surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Color.Black,*/
)

@Composable
fun ComposePlaygroundTheme(isDarkTheme: Boolean = false, content: @Composable() () -> Unit) {
    val colors = if (isDarkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = typography,
        shapes = shapes,
        content = content
    )
}
