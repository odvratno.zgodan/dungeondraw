import SwiftUI
import shared



struct ContentView: View {
    @ObservedObject var appViewModel = AppViewModel()
    
    var body: some View {
//        Text("posts count: \(appViewModel.appState.posts.count)")
        
        NavigationView {
            VStack {
                listView()
                .navigationBarTitle(appViewModel.appState.loading ? "Loading..." : "Posts")
                
                
            }
        }
    }
    
    private func listView() -> AnyView {
        AnyView(List(appViewModel.appState.posts) { post in
            FlickrImageRow(flickrImage: post)
        })
    }
}

struct FlickrImageRow: View {
    var flickrImage: FlickrImage

    var body: some View {
        HStack() {
            VStack(alignment: .leading, spacing: 10.0) {
                Text(flickrImage.title)
            }
            Spacer()
        }
    }
}

extension FlickrImage: Identifiable { }

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


class AppViewModel: ObservableObject {
    let sharedModel : FlickrViewModel = FlickrViewModel()
    @Published var appState : FlickrAppState = FlickrAppState(posts: [FlickrImage](), detail: nil, loading: false, error: false)
    init() {
        sharedModel.onChange { newState in
            self.appState = newState
        }
        sharedModel.loadData()
    }
}
