import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat

// ////////////////////////////////////////////////////////
/*
THE DESKTOP MODULE REQUIRES THAT THE PROJECT JAVA SDK
IS SET TO 14 OR HIGHER
*/
// ////////////////////////////////////////////////////////

plugins {
    kotlin("multiplatform")
    id("org.jetbrains.compose") version Versions.compose
}

group = "no.none.dungeondraw"
version = "1.0.11"

kotlin {
    jvm {
        compilations.all {
            kotlinOptions {
                useIR = true
                jvmTarget = "11"
            }
        }
    }
    sourceSets {
        val jvmMain by getting {
            dependencies {
                implementation(compose.desktop.currentOs)
                implementation(project(":shared"))
                implementation(project(":sharedCompose"))
            }
        }
        val jvmTest by getting
    }
}

compose.desktop {
    application {
        mainClass = "MainKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg/*, TargetFormat.Msi, TargetFormat.Deb*/)
            packageName = "DungeonDraw"
            packageVersion = "1.0.11" // "${rootProject.version}" // must not start with '0'
            description = "Dungeon&Dragons utility"
            copyright = "2020 Bruno Blazinc"
            vendor = "Bruno Blazinc"
            modules("java.instrument", "java.naming", "java.sql", "jdk.unsupported")

            val iconsRoot = project.file("../shared/src/jvmMain/resources/images")
            macOS {
                bundleID = "no.none.dungeondraw"
                signing {
                    sign.set(true)
                    identity.set("DECODE HQ Ltd. (Y8Z27FN9Z7)")
                }
                notarization {
                    appleID.set("bruno.blazinc@decode.agency")
                    password.set("@keychain:NOTARIZATION_PASSWORD")
                    ascProvider.set("MarkoStrizic297878341")
                }
                iconFile.set {
                    iconsRoot.resolve("icon-mac.icns")
                }
            }
            windows {
                iconFile.set {
                    iconsRoot.resolve("icon-windows.ico")
                }
                menuGroup = "DungeonDraw"
                // see https://wixtoolset.org/documentation/manual/v3/howtos/general/generate_guids.html
                upgradeUuid = "e51a9276-08c6-43dd-81be-c2b219143382"
            }
            linux {
                iconFile.set {
                    iconsRoot.resolve("icon-linux.png")
                }
            }
        }
    }
}
