import androidx.compose.desktop.AppManager
import androidx.compose.desktop.DesktopTheme
import androidx.compose.desktop.Window
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.DrawerValue
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.ModalDrawer
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Redo
import androidx.compose.material.icons.filled.Undo
import androidx.compose.material.rememberDrawerState
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import no.none.dungeondraw.compose.ui.MapDetails
import no.none.dungeondraw.compose.ui.MapDrawerDrawingControls
import no.none.dungeondraw.compose.ui.MapLists
import no.none.dungeondraw.compose.ui.PaintView
import no.none.dungeondraw.compose.ui.primary
import no.none.dungeondraw.shared.data.navigation.MainScreen
import no.none.dungeondraw.shared.data.viewmodel.MapsViewModel
import no.none.dungeondraw.shared.data.viewmodel.NavigationViewModel
import no.none.dungeondraw.shared.data.viewmodel.deleteMapFingerPath
import no.none.dungeondraw.shared.data.viewmodel.drawMap
import no.none.dungeondraw.shared.data.viewmodel.editMap
import no.none.dungeondraw.shared.data.viewmodel.goBack
import no.none.dungeondraw.shared.data.viewmodel.loadAllMaps
import no.none.dungeondraw.shared.data.viewmodel.loadMapDetails
import no.none.dungeondraw.shared.data.viewmodel.newMap
import no.none.dungeondraw.shared.data.viewmodel.saveMapDetails
import no.none.dungeondraw.shared.data.viewmodel.saveMapFingerPath
import no.none.dungeondraw.shared.data.viewmodel.setRenderedImagePath
import no.none.dungeondraw.shared.data.viewmodel.startServerDiscovery
import no.none.dungeondraw.shared.data.viewmodel.startUDPServer
import no.none.dungeondraw.shared.data.viewmodel.stopServerDiscovery
import no.none.dungeondraw.shared.data.viewmodel.stopUDPServer
import no.none.dungeondraw.shared.data.vo.FingerPath
import no.none.dungeondraw.shared.data.vo.Map
import no.none.dungeondraw.shared.data.vo.Result
import org.jetbrains.skija.Image
import java.awt.Desktop
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.io.File
import java.net.URI
import javax.imageio.ImageIO
import javax.swing.JFileChooser

private val mapsViewModel by lazy {
    MapsViewModel()
}

private val navigationViewModel by lazy {
    NavigationViewModel()
}

private val mapImagePath: MutableState<String> = mutableStateOf("")

@ExperimentalCoroutinesApi
fun main() = Window {
    val applicationCacheFile = getApplicationCachesPath()
    val networkImagesFolder = File(applicationCacheFile.path + "/networkImages")
    if (!networkImagesFolder.exists()) {
        try {
            networkImagesFolder.mkdir()
        } catch (e: Exception) {
            println("e:$e")
        }
    }
    DesktopTheme {
        val navigationState = navigationViewModel.stateFlow.collectAsState()

        when (navigationState.value.currentScreen) {
            MainScreen.LIST, MainScreen.NEW, MainScreen.EDIT -> {
                MapDetailsScreen(
                    navigationViewModel,
                    mapsViewModel
                )
            }
            MainScreen.DRAW -> MapDrawScreen(
                navigationViewModel,
                mapsViewModel
            )
        }

        if (navigationState.value.currentScreen == MainScreen.LIST) {
            mapsViewModel.startServerDiscovery()
        } else {
            mapsViewModel.stopServerDiscovery()
        }
    }
}

fun loadImagePath() {
    val current = AppManager.focusedWindow
    if (current != null) {
        val jFrame = current.window
        // Do whatever you want with it
        val fileChooser = JFileChooser()
        fileChooser.currentDirectory = File(System.getProperty("user.home"))
        fileChooser.fileSelectionMode = JFileChooser.OPEN_DIALOG
        val result = fileChooser.showOpenDialog(jFrame)
        if (result == JFileChooser.APPROVE_OPTION) {

            mapImagePath.value = copyImage(fileChooser.selectedFile).absolutePath
        }
    }
}

fun loadImageBitmap(path: String): ImageBitmap {
    val file = File(path)
    println("path:$path")

    val inputStream = file.inputStream()
    val bufferedImage = ImageIO.read(inputStream)

    val stream = ByteArrayOutputStream()
    ImageIO.write(bufferedImage, "png", stream)
    val byteArray = stream.toByteArray()

    return Image.makeFromEncoded(byteArray).asImageBitmap()
}

fun getApplicationCachesPath(): File {
    val osName = System.getProperty("os.name").toLowerCase()
    val file = when {
        osName.indexOf("windows") > -1 -> {
            File(System.getProperty("user.home") + "/Library/Caches/DungeonDraw")
        }
        osName.indexOf("mac") > -1 -> {
            File(System.getProperty("user.home") + "/Library/Caches/DungeonDraw")
        }
        osName.indexOf("nix") > -1 -> {
            File(System.getProperty("user.home") + "/.cache/DungeonDraw")
        }
        else -> { // anything else
            File(System.getProperty("user.home") + "/caches/DungeonDraw")
        }
    }
    if (!file.exists()) {
        try {
            file.mkdir()
        } catch (e: Exception) {
            println("e:$e")
        }
    }
    return file
}

fun saveImage(imageBitmap: ImageBitmap): String {
    val applicationCacheFile = getApplicationCachesPath()
    val networkImagesFolder = File(applicationCacheFile.path + "/networkImages")
    if (!networkImagesFolder.exists()) {
        try {
            networkImagesFolder.mkdir()
        } catch (e: Exception) {
            println("e:$e")
        }
    }
    val file = File(networkImagesFolder.path + "/saved_image.jpeg")
    val buffer = IntArray(imageBitmap.width * imageBitmap.height)
    imageBitmap.readPixels(buffer)
    val bufferedImage = BufferedImage(imageBitmap.width, imageBitmap.height, BufferedImage.TYPE_INT_RGB)
    bufferedImage.setRGB(0, 0, imageBitmap.width, imageBitmap.height, buffer, 0, bufferedImage.width)
    val result = ImageIO.write(bufferedImage, "jpg", file)
    println("bufferedImage.width:${bufferedImage.width}, bufferedImage.height:${bufferedImage.height}, result:$result")
    return file.absolutePath
}

fun copyImage(imageFile: File): File {
    val applicationCacheFile = getApplicationCachesPath()
    val copiedImagesFolder = File(applicationCacheFile.path + "/images")
    if (!copiedImagesFolder.exists()) {
        try {
            copiedImagesFolder.mkdir()
        } catch (e: Exception) {
            println("Exception while copying file. e:$e")
        }
    }
    val file = File(copiedImagesFolder.path + "/" + imageFile.name)
    return imageFile.copyTo(file, true)
}

@ExperimentalCoroutinesApi
@Composable
fun MapDrawScreen(navigationViewModel: NavigationViewModel, mapsViewModel: MapsViewModel) {
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
    val drawerState = rememberDrawerState(DrawerValue.Closed)
    val scope = rememberCoroutineScope()

    val map = mapsViewModel.stateFlow.collectAsState().value.map
    val serverAddress = mapsViewModel.stateFlow.collectAsState().value.serverAddress
    val fingerPathsResult = mapsViewModel.stateFlow.collectAsState().value.mapFingerPaths
    val undoFingerPaths = remember { mutableStateOf(listOf<FingerPath>()) }
    var mapImage: ImageBitmap? = null
    if (mapImagePath.value.isNotEmpty()) {
        mapImage = loadImageBitmap(mapImagePath.value)
    }

    var mapName = ""
    if (map is Result.Success) {
        mapName = map.data.name
    }

    var serverAddressString = ""
    if (serverAddress is Result.Success) {
        serverAddressString = "http://${serverAddress.data.removePrefix("/")}:8080"
    }

    val iconModifier = Modifier.size(16.dp)

    val showMasked = remember { mutableStateOf(false) }
    val brushSize = remember { mutableStateOf(18f) }
    val defaultPathStyle = remember { mutableStateOf(FingerPath.Style.FILL) }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        "$mapName at $serverAddressString", color = Color.White,
                        modifier = Modifier.clickable {
                            Desktop.getDesktop().browse(URI.create(serverAddressString))
                        }
                    )
                },
                backgroundColor = primary,
                actions = {
                    if (map is Result.Success && fingerPathsResult is Result.Success) {
                        Button(
                            onClick = {
                                val undoOriginal = undoFingerPaths.value.toMutableList()
                                fingerPathsResult.data.last().apply {
                                    undoOriginal.add(this)
                                    undoFingerPaths.value = undoOriginal
                                    mapsViewModel.deleteMapFingerPath(this)
                                }
                            },
                            enabled = fingerPathsResult.data.isNotEmpty(),
                            shape = CircleShape,
                            modifier = Modifier.padding(8.dp).sizeIn(8.dp, 8.dp),
                            contentPadding = PaddingValues(6.dp)
                        ) {
                            Icon(Icons.Filled.Undo, "", modifier = iconModifier)
                        }

                        Button(
                            onClick = {
                                val undoOriginal = undoFingerPaths.value.toMutableList()
                                undoOriginal.removeLast().apply {
                                    this.id = 0L
                                    undoFingerPaths.value = undoOriginal
                                    mapsViewModel.saveMapFingerPath(this)
                                }
                            },
                            enabled = undoFingerPaths.value.isNotEmpty(),
                            shape = CircleShape,
                            modifier = Modifier.padding(8.dp).sizeIn(8.dp, 8.dp),
                            contentPadding = PaddingValues(6.dp)
                        ) {
                            Icon(Icons.Filled.Redo, "", modifier = iconModifier)
                        }
                    }
                },
                navigationIcon = {
                    IconButton(
                        onClick = {
                            scope.launch {
                                if (drawerState.isClosed) {
                                    drawerState.open()
                                } else {
                                    drawerState.close()
                                }
                            }
                        }
                    ) {
                        Icon(Icons.Default.Menu, contentDescription = null)
                    }
                }
            )
        },
        content = {
            ModalDrawer(
                drawerState = drawerState,
                gesturesEnabled = false,
                drawerContent = {
                    if (map is Result.Success && fingerPathsResult is Result.Success) {
                        MapDrawerDrawingControls(
                            brushSize = brushSize,
                            showMasked = showMasked,
                            defaultPathStyle = defaultPathStyle,
                            onNavigateBackClick = {
                                navigationViewModel.goBack()
                            },
                            drawerChanged = {
                                scope.launch {
                                    if (it) drawerState.open() else drawerState.close()
                                }
                            }
                        )
                    }
                },
                content = {
                    Column {
                        if (map is Result.Success && fingerPathsResult is Result.Success) {
                            PaintView(
                                map.data.id,
                                fingerPathsResult.data,
                                undoFingerPaths,
                                mapImage,
                                brushSize = brushSize,
                                showMasked = showMasked,
                                defaultPathStyle = defaultPathStyle,
                                onFingerDraw = { path ->
                                    println(">>> MainActivity.onFingerDraw() path.id:${path.id}")
                                    mapsViewModel.saveMapFingerPath(path)
                                },
                                onImageDraw = { image ->
                                    println(">>> MainActivity.onImageDraw()")
                                    val path = saveImage(image)
                                    mapsViewModel.setRenderedImagePath(path)
                                }
                            )
                        }
                    }
                }
            )
        }
    )
    mapsViewModel.startUDPServer()
}

@ExperimentalCoroutinesApi
@Composable
fun MapDetailsScreen(navigationViewModel: NavigationViewModel, mapsViewModel: MapsViewModel) {
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
    val navigationState = navigationViewModel.stateFlow.collectAsState()

    val map =
        if (mapsViewModel.stateFlow.collectAsState().value.map is Result.Success) (mapsViewModel.stateFlow.collectAsState().value.map as Result.Success).data else Map()
    val mapName = remember { mutableStateOf(map.name) }
    var mapImage: ImageBitmap? = null

    var topAppBarTitle = ""
    if (navigationState.value.currentScreen == MainScreen.LIST) {
        topAppBarTitle = "DungeonDraw"
    } else if (mapName.value.isNotEmpty()) {
        topAppBarTitle = mapName.value
    }

    if (map.id != 0L) {
        if (map.imagePath.isNotEmpty()) {
            mapImagePath.value = map.imagePath
        }
        if (map.name.isNotEmpty()) {
            mapName.value = map.name
        }
    }

    if (mapImagePath.value.isNotEmpty()) {
        mapImage = loadImageBitmap(mapImagePath.value)
    }

    println("map.id:${map.id}, map.name${map.name}")

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = { Text(topAppBarTitle, color = Color.White) },
                backgroundColor = primary,
                actions = {
                    when (navigationState.value.currentScreen) {
                        MainScreen.LIST -> {
                            IconButton(
                                onClick = {
                                    navigationViewModel.newMap()
                                }
                            ) {
                                Icon(Icons.Default.Add, contentDescription = null)
                            }
                        }
                        MainScreen.NEW, MainScreen.EDIT -> {
                            if (map.id != 0L) {
                                IconButton(
                                    onClick = {
                                        mapsViewModel.loadMapDetails(map)
                                        navigationViewModel.drawMap()
                                    }
                                ) {
                                    Icon(Icons.Default.Edit, contentDescription = null)
                                }
                            }
                            IconButton(
                                onClick = {
                                    map.name = mapName.value
                                    map.imagePath = mapImagePath.value
                                    mapImagePath.value = ""
                                    mapsViewModel.saveMapDetails(map)
                                    navigationViewModel.goBack()
                                }
                            ) {
                                Icon(Icons.Default.Done, contentDescription = null)
                            }
                        }
                    }
                }
            )
        },
        content = {

            Row(Modifier.fillMaxSize()) {

                Box(Modifier.width(320.dp).fillMaxHeight()) {
                    MapLists(
                        mapsViewModel,
                        onSelected = {
                            mapsViewModel.loadMapDetails(it)
                            navigationViewModel.editMap()
                        }
                    )
                }

                Spacer(modifier = Modifier.width(1.dp).fillMaxHeight().background(Color.LightGray))
                when (navigationState.value.currentScreen) {
                    MainScreen.NEW, MainScreen.EDIT -> Box(Modifier.fillMaxHeight()) {
                        MapDetails(
                            mapName = mapName, mapImage = mapImage,
                            imageButtonOnClick = {
                                loadImagePath()
                            }
                        )
                    }
                }
            }
        }
    )

    if (navigationState.value.currentScreen == MainScreen.LIST) {
        mapImagePath.value = ""
        mapsViewModel.loadAllMaps()
    }
    mapsViewModel.stopUDPServer()
}
