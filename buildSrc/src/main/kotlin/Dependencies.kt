object Versions {
    // Latest Kotlin compiler version is not yet supported by Compose so we have to use 1.4.21-2
    const val kotlin = "1.5.10"
    const val kotlinSerializationPlugin = "1.5.0"
    const val kotlinxDatetime = "0.1.1"
    const val kotlinCoroutines = "1.4.2-native-mt"
    const val sqlDelight = "1.5.0"

    const val ktx = "1.3.2"
    const val appCompat = "1.3.0-rc01"
    const val material = "1.2.1"
    const val activity = "1.2.0-rc01"
    const val activityCompose = "1.3.0-alpha03"

    const val lifecycle = "2.2.0"
    const val lifecycleRuntime = "2.3.0-rc01"

    const val ktor = "1.5.0"
    const val kotlinxSerialization = "1.0.1"

    const val sqliteJdbcDriver = "3.34.0"
    const val slf4j = "1.7.30"

    const val junit = "4.13"
    const val testRunner = "1.3.0"

    const val compose = "0.4.0-rc2"
    const val detekt = "1.16.0-RC2"
    const val ktlintGradle = "10.0.0"
    const val fontAwesome = "0.2.0"
}


object AndroidSdk {
    const val min = 26
    const val compile = 30
    const val target = compile
}

object Test {
    const val junit = "junit:junit:${Versions.junit}"
}

object Compose {
    // Compose definitions are inside sharedCompose module
    const val iconsExtended = "androidx.compose.material:material-icons-extended:${Versions.compose}"
    const val iconsFontAwesome = "br.com.devsrsouza.compose.icons.jetbrains:font-awesome:${Versions.fontAwesome}"
}

object Coroutines {
    const val coroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.kotlinCoroutines}"
    const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.kotlinCoroutines}"
}


object Ktor {
    const val clientCore = "io.ktor:ktor-client-core:${Versions.ktor}"
    const val clientSerialization = "io.ktor:ktor-client-serialization:${Versions.ktor}"

    const val clientAndroid = "io.ktor:ktor-client-android:${Versions.ktor}"

    const val clientApache = "io.ktor:ktor-client-apache:${Versions.ktor}"
    const val slf4j = "org.slf4j:slf4j-simple:${Versions.slf4j}"
    const val clientIos = "io.ktor:ktor-client-ios:${Versions.ktor}"
    const val clientCio = "io.ktor:ktor-client-cio:${Versions.ktor}"
    const val clientJs = "io.ktor:ktor-client-js:${Versions.ktor}"

    const val serverJetty = "io.ktor:ktor-server-jetty:1.4.1"
    const val ktorGson = "io.ktor:ktor-gson:1.4.1"
}

object Serialization {
    const val core = "org.jetbrains.kotlinx:kotlinx-serialization-core:${Versions.kotlinxSerialization}"
}

object SqlDelight {
    const val runtime = "com.squareup.sqldelight:runtime:${Versions.sqlDelight}"
    val coroutineExtensions = "com.squareup.sqldelight:coroutines-extensions:${Versions.sqlDelight}"
    val androidDriver = "com.squareup.sqldelight:android-driver:${Versions.sqlDelight}"

    val nativeDriver = "com.squareup.sqldelight:native-driver:${Versions.sqlDelight}"
    val sqlliteDriver = "com.squareup.sqldelight:sqlite-driver:${Versions.sqlDelight}"
}

object Androidx {
    val ktxCore = "androidx.core:core-ktx:${Versions.ktx}"
    val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    val activity = "androidx.activity:activity-ktx:${Versions.activity}"
    val activityCompose = "androidx.activity:activity-compose:${Versions.activityCompose}"
}

object Material {
    val material = "com.google.android.material:material:${Versions.material}"
}

object Lifecycle {
    val runtime = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycleRuntime}"
    val viewmodel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    val extensions = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle}"
}

object Kotlinx {
    val datetime = "org.jetbrains.kotlinx:kotlinx-datetime:${Versions.kotlinxDatetime}"
    val datetimeDesugarJdk = "com.android.tools:desugar_jdk_libs:1.1.1"
}

object Logging {
    val napier = "com.github.aakira:napier:1.5.0-alpha1"
}

