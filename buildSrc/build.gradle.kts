repositories {
    jcenter()
}

plugins {
    `kotlin-dsl`
}

kotlin {
    // Add Deps to compilation, so it will become available in main project
    sourceSets.getByName("main").kotlin.srcDir("buildSrc/src/main/kotlin")
}